#!/bin/bash
# An autobuild script! Work in progress.

cd /root/sage-auto

echo " * Welcome to AIMS Sagemath PPA uploader, retrieving information..."

# First, let's set some constants
SAGEVERSION=7.3
export DEBEMAIL="root+sagemath@aims.ac.za"
export DEBFULLNAME="AIMS Sagemath Autobuilder"
#SAGEMIRROR="ftp://ftp.fu-berlin.de/unix/misc/sage/linux/" # takes long before mirroring
#SAGEMIRROR="https://fourdots.com/mirror/sagemath/linux/" # slow bandwidth
SAGEMIRROR="http://files.sagemath.org/linux" # Binaries first uploaded here
#SAGEMIRROR_32BIT="$SAGEMIRROR/32bit/"
SAGEMIRROR_64BIT="$SAGEMIRROR/64bit/"
#UBUNTU1404_32_TARBALL=$(wget -qO - $SAGEMIRROR_32BIT | grep \>sage-$SAGEVERSION-Ubuntu_14.04 | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' |  awk '{print $6}')
UBUNTU1404_64_TARBALL=$(wget -qO - $SAGEMIRROR_64BIT | grep \>sage-$SAGEVERSION-Ubuntu_14.04 | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' |  awk '{print $6}')
#DEBIAN8_32_TARBALL=$(wget -qO - $SAGEMIRROR_32BIT | grep \>sage.*Debian_GNU_Linux_testing | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' | awk '{print $6}')
DEBIAN9_64_TARBALL=$(wget -qO - $SAGEMIRROR_64BIT | grep \>sage.*Debian_GNU_Linux_testing | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' | awk '{print $6}')

#echo "   Ubuntu 14.04 32 bit tarball is: $UBUNTU1404_32_TARBALL"
echo "   Ubuntu 14.04 64 bit tarball is: $UBUNTU1404_64_TARBALL"
#echo "       Debian 8 32 bit tarball is: $DEBIAN8_32_TARBALL (currently not built)"
echo "       Debian 9 64 bit tarball is: $DEBIAN8_64_TARBALL"

echo " * Fetching tarballs..."
mkdir -p tmp
cd tmp
#wget -cq $SAGEMIRROR_32BIT/$UBUNTU1404_32_TARBALL && \
#echo "   File downloaded at size $(du -sh $UBUNTU1404_32_TARBALL)"
wget -cq $SAGEMIRROR_64BIT/$UBUNTU1404_64_TARBALL && \
echo "   File downloaded at size $(du -sh $UBUNTU1404_64_TARBALL)"
wget -cq $SAGEMIRROR_64BIT/$DEBIAN9_64_TARBALL && \
echo "   File downloaded at size $(du -sh $DEBIAN9_64_TARBALL)"

echo " * Processing tarballs..."
#tar -xjf $UBUNTU1404_32_TARBALL
#mv SageMath i386

# TODO: The block(s) below should be turned into functions
#(
#    tar -xjf $UBUNTU1404_64_TARBALL
#    mv SageMath amd64
#
#    # Fix-ups for  packaging...
#    mv amd64/{.git,git}
#    mv amd64/build/{.gitignore,gitignore}
#    mv amd64/src/{.gitignore,gitignore}
#    cd ..
#
#    echo " * Fetching packaging from git..."
#    # Remove existing directory if it exists
#    rm -rf sagemath-upstream-binary
#    git  clone git@bitbucket.org:aims_za/sagemath-upstream-binary.git
#    mv tmp/amd64 sagemath-upstream-binary/sagemath-upstream-binary
#    rm -rf tmp
#    cd sagemath-upstream-binary/sagemath-upstream-binary
#
#    # Run relocate-once.py from https://github.com/sagemath/binary-pkg to patch absolute paths in binaries
#    cd amd64
#    python relocate-once.py -d /usr/lib/sagemath/
#
#    echo " * Creating changelog entry..."
#    DATE=$(date +%Y%m%d%H%M%S)
#    dch  --force-distribution "Automated QA Upload" -D trusty -l ~qa$DATE
#
#    echo " * Creating source package..."
#    dpkg-buildpackage -S
#    cd ..
#    dput aims-sage-dev sagemath*changes
#    mv sagemath-upstream-binary /root/sage-auto/sagemath-upstream-binary-$DATE
#    echo "If you would like to modify/upload this version to production, you can find it in sagemath-upstream-binary-$DATE"
#    echo " * Removing working directory..."
#    cd ../../
#    rm -rf /root/sage-auto/sagemath-upstream-binary
#)
# Fix-ups for  packaging...
mv amd64/{.git,git}
mv amd64/build/{.gitignore,gitignore}
mv amd64/build/make/{.gitignore,gitignore}
mv amd64/src/{.gitignore,gitignore}
mv amd64/src/doc/{.gitignore,gitignore}

#mv i386/{.git,git}
#mv i386/build/{.gitignore,gitignore}
#mv i386/src/{.gitignore,gitignore}
#mkdir i386/local/lib64
#echo "This directory is required for i386 packaging, but not used." > i386/local/lib64/README.txt
cd ..


(
    tar -xjf $DEBIAN9_64_TARBALL
    mv SageMath amd64

    # Fix-ups for  packaging...
    mv amd64/{.git,git}
    mv amd64/build/{.gitignore,gitignore}
    mv amd64/src/{.gitignore,gitignore}
    cd ..

    echo " * Fetching packaging from git..."
    # Remove existing directory if it exists
    rm -rf sagemath-upstream-binary
    git  clone git@bitbucket.org:aims_za/sagemath-upstream-binary.git
    mv tmp/amd64 sagemath-upstream-binary/sagemath-upstream-binary
    rm -rf tmp
    cd sagemath-upstream-binary/sagemath-upstream-binary

    # Run relocate-once.py from https://github.com/sagemath/binary-pkg to patch absolute paths in binaries
    cd amd64
    python relocate-once.py -d /usr/lib/sagemath/

    echo " * Creating changelog entry..."
    DATE=$(date +%Y%m%d%H%M%S)
    dch  --force-distribution "Automated QA Upload" -D unstable -l ~qa$DATE

    echo " * Creating package(s)..."
    dpkg-buildpackage
    cd ..
    dput aims-sage-dev sagemath*changes
    mv sagemath-upstream-binary /root/sage-auto/sagemath-upstream-binary-$DATE
    echo "If you would like to modify/upload this version to production, you can find it in sagemath-upstream-binary-$DATE"
    echo " * Removing working directory..."
    cd ../../
    rm -rf /root/sage-auto/sagemath-upstream-binary
)

