#!/bin/bash

SAGEVERSION=7.4

export DEBEMAIL="jan@aims.ac.za"
export DEBFULLNAME="Jan Groenewald"

#SAGEMIRROR="ftp://ftp.fu-berlin.de/unix/misc/sage/linux/" # takes long before mirroring
#SAGEMIRROR="https://fourdots.com/mirror/sagemath/linux/" # slow bandwidth
#SAGEMIRROR="http://files.sagemath.org/linux" # Binaries first uploaded here
#SAGEMIRROR_64BIT="$SAGEMIRROR/64bit/"
SAGEMIRROR_64BIT="http://mirror.switch.ch/mirror/sagemath/linux/64bit/"

UBUNTU1404_64_TARBALL=$(wget -qO - $SAGEMIRROR_64BIT | grep \>sage-$SAGEVERSION-Ubuntu_14.04 | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' |  awk '{print $6}')

echo "   Ubuntu 14.04 64 bit tarball is: $UBUNTU1404_64_TARBALL"
echo " * Fetching tarballs..."

mkdir -p tmp
cd tmp
wget -cq $SAGEMIRROR_64BIT/$UBUNTU1404_64_TARBALL && \
echo "   File downloaded at size $(du -sh $UBUNTU1404_64_TARBALL)"

echo " * Processing tarballs..."
tar -xjf $UBUNTU1404_64_TARBALL
mv SageMath amd64

# Fix-ups for  packaging...
mv amd64/{.git,git}
mv amd64/build/{.gitignore,gitignore}
mv amd64/src/{.gitignore,gitignore}
cd ..

echo " * Fetching packaging from git..."
# Remove existing directory if it exists
rm -rf sagemath-upstream-binary
git clone git@bitbucket.org:aims_za/sagemath-upstream-binary.git
mv tmp/amd64 sagemath-upstream-binary/sagemath-upstream-binary
rm -rf tmp
cd sagemath-upstream-binary/sagemath-upstream-binary

# Run relocate-once.py from https://github.com/sagemath/binary-pkg to patch absolute paths in binaries
cd amd64
python relocate-once.py -d /usr/lib/sagemath/
#cd ../i386
#python relocate-once.py -d /usr/lib/sagemath/
#cd ..

echo " * Creating changelog entry..."
DATE=$(date +%Y%m%d%H%M%S)
dch  --force-distribution "Automated QA Upload" -D trusty -l ~qa$DATE

echo " * Creating source package..."
dpkg-buildpackage -S
cd ..
dput aims-sage-dev sagemath*changes
mv sagemath-upstream-binary /root/sage-auto/sagemath-upstream-binary-$DATE
echo "If you would like to modify/upload this version to production, you can find it in sagemath-upstream-binary-$DATE"
echo " * Removing working directory..."
cd ../../
rm -rf /root/sage-auto/sagemath-upstream-binary

